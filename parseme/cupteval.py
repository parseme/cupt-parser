#! /usr/bin/env python3

#from typing import Union, NamedTuple, Dict, Set, Iterable, Optional
#import parseme.cupt as cupt
#from conllu import TokenList
import collections
import sys

_MAX_WARNINGS = 10
_WARNED = collections.defaultdict(int)
COLOR_STDERR = True

def warn(message, *, warntype="WARNING", position=None, **format_args):
    _WARNED[message] += 1
    if _WARNED[message] <= _MAX_WARNINGS:
        if position is None:
            position = "{}:{}: ".format(last_filename, last_lineno) if last_filename else ""
        msg_list = message.format(**format_args).split("\n")
        if _WARNED[message] == _MAX_WARNINGS:
            msg_list.append("(Skipping following warnings of this type)")

        line_beg, line_end = ('\x1b[31m', '\x1b[m') if COLOR_STDERR else ('', '')
        for i, msg in enumerate(msg_list):
            warn = warntype if i==0 else "."*len(warntype)
            print(line_beg, position, warn, ": ", msg, line_end, sep="", file=sys.stderr)
